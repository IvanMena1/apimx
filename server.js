var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');
var movimientosv2JSON = require('./movimientosv2.json');

var bodyparser = require('body-parser');
app.use(bodyparser.json());

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get('/', function(req, res) {
  res.sendFile(path.join(__dirname, 'index.html'));
});

app.post('/', function(req, res) {
  res.send('Hemos recibido su petición POST');
});

app.put('/', function(req, res) {
  res.send('Hemos recibido su petición PUT');
});

app.delete('/', function(req, res) {
  res.send('Hemos recibido su petición DELETE cambiada');
});

app.get('/Clientes/:idCliente', function(req, res) {
  res.send('Aquí tiene al cliente número: ' + req.params.idCliente);
});

app.get('/v1/Movimientos', function(req, res) {
  res.sendfile('movimientosv1.json');
});

app.get('/v2/Movimientos', function(req, res) {
  res.json(movimientosv2JSON);
});

app.get('/v2/Movimientos/:idMovimiento', function(req, res) {
  console.log(req.params.idMovimiento);
  res.send(movimientosv2JSON[req.params.idMovimiento]);
});

app.get('/v2/Movimientosquery', function(req, res) {
  console.log(req.query);
  res.send('Recibido: ' + req.query.nombre);
})

app.post('/v2/Movimientos', function(req, res) {
  var nuevo = req.body;
  nuevo.id = movimientosv2JSON.length + 1;
  movimientosv2JSON.push(nuevo);
  res.send('Movimiento ha sido registrado con id: ' + nuevo.id);
})
